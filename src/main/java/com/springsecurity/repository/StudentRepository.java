package com.springsecurity.repository;

import com.springsecurity.model.dao.StudentResponse;
import org.apache.ibatis.annotations.*;

import java.util.Optional;

@Mapper
public interface StudentRepository {
    @Select("SELECT * FROM STUDENT WHERE name = #{name};")
    @ResultMap("studentResultMap")
    Optional<StudentResponse> retrieveStudentByUsername (@Param("name") String name);

    @Select("SELECT * FROM STUDENT WHERE id = #{id};")
    @Results(id = "studentResultMap", value = {
            @Result(property = "id",          column = "id"       ),
            @Result(property = "name",        column = "name"     ),
            @Result(property = "gender",      column = "gender"   ),
            @Result(property = "dateOfBirth", column = "dob"      ),
            @Result(property = "phone",       column = "phone"    ),
            @Result(property = "email",       column = "email"    ),
            @Result(property = "address",     column = "address"  ),
            @Result(property = "imageUrl",    column = "image_url"),
            @Result(property = "dateOfJoin",  column = "doj"      ),
            @Result(property = "status",             column = "status"   ),
            @Result(property = "parentId",           column = "p_id"     )
    })
    StudentResponse retrieveStudentById (@Param ("id") int id);

}
