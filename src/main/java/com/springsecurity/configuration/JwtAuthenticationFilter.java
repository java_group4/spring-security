package com.springsecurity.configuration;

import com.springsecurity.services.jwt.JwtService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {
    private final JwtService _service;
    private final UserDetailsService _userDetails;
    @Override
    protected void doFilterInternal(
            @NonNull HttpServletRequest request,
            @NonNull HttpServletResponse response,
            @NonNull FilterChain filterChain) throws ServletException, IOException , NullPointerException{
        final String _authHeader = request.getHeader("Authorization");
        final String _jwt;
        final String _username;
        if (_authHeader != null || !_authHeader.startsWith("Bearer ")) {
            filterChain.doFilter(request, response);
            return;
        }
        _jwt = _authHeader.substring(7);
        _username = _service.getAccessToken(_jwt);
        if (_username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails _user = this._userDetails.loadUserByUsername(_username);
            if (_service.isTokenValid(_jwt, _user)) {
                UsernamePasswordAuthenticationToken _authToken = new UsernamePasswordAuthenticationToken(
                        _user,
                        null,
                        _user.getAuthorities()
                );
                _authToken.setDetails(
                        new WebAuthenticationDetailsSource()
                                .buildDetails(request)
                );
                SecurityContextHolder.getContext().setAuthentication(_authToken);
            }
        }
        filterChain.doFilter(request, response);
    }
}
