package com.springsecurity.configuration;

import com.springsecurity.repository.StudentRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@AllArgsConstructor
public class AuthConfiguration {
    private final StudentRepository _repo;
    @Bean
    public UserDetailsService userDetailsService () {
        return (_username) -> _repo.retrieveStudentByUsername(_username)
                .orElseThrow(() -> new UsernameNotFoundException("Username not found."));
    }

    @Bean
    public AuthenticationProvider authenticationProvider () {
        DaoAuthenticationProvider _daoProvider = new DaoAuthenticationProvider();
        _daoProvider.setUserDetailsService(userDetailsService());
        _daoProvider.setPasswordEncoder(passwordEncoder());
        return _daoProvider;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationManager authenticationManager (AuthenticationConfiguration configuration) throws Exception{
        return configuration.getAuthenticationManager();
    }
}
