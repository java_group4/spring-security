package com.springsecurity.services.student;

import com.springsecurity.model.dao.AuthenticationResponse;
import com.springsecurity.model.dto.AuthenticationRequest;

public interface StudentService {

    AuthenticationResponse authenticate (AuthenticationRequest authRequest);

}
