package com.springsecurity.services.student;

import com.springsecurity.constants.Constant;
import com.springsecurity.model.dao.AuthenticationResponse;
import com.springsecurity.model.dto.AuthenticationRequest;
import com.springsecurity.repository.StudentRepository;
import com.springsecurity.services.jwt.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class StudentServiceImpl implements StudentService{
    private final StudentRepository _studentRepo;
    private final AuthenticationManager _authManager;
    private final JwtService _jwtService;
    @Override
    public AuthenticationResponse authenticate(AuthenticationRequest authRequest) {
        _authManager.authenticate(new UsernamePasswordAuthenticationToken(
                authRequest.getName(),
                authRequest.getEmail()
        ));
        var _userData = _studentRepo.retrieveStudentByUsername(authRequest.getName())
                .orElseThrow();
        var _tokenGenerated = _jwtService.generateToken(_userData);
        return AuthenticationResponse.builder()
                .accessToken(_tokenGenerated)
                .expiration(Constant.EXPIRATION)
                .build();
    }
}
