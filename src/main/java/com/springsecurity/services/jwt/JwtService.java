package com.springsecurity.services.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static com.springsecurity.constants.Constant.EXPIRATION;

@Service
public class JwtService {
    private static final String SECRET_KEY = "432646294A404E635266556A576E5A7234753778214125442A472D4B61506453";
    public String getAccessToken(String token) {
        return extractClaims(token, Claims::getSubject);
    }

    private Claims extractAllClaims (String token) {
        return Jwts.parserBuilder()
                .setSigningKey(getSigningKey())
                .build()
                .parseClaimsJwt(token)
                .getBody();
    }

    private Key getSigningKey() {
        byte[] _keyByte = Decoders.BASE64.decode(SECRET_KEY);
        return Keys.hmacShaKeyFor(_keyByte);
    }

    public <T> T extractClaims(String token, Function<Claims, T> claimsTFunction) {
        final Claims _claims = extractAllClaims(token);
        return claimsTFunction.apply(_claims);
    }

    private String generateToken (Map<String, Object> extractClaim, UserDetails userDetails) {
        return Jwts.builder()
                .setClaims(extractClaim)
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(EXPIRATION))
                .signWith(getSigningKey(), SignatureAlgorithm.HS256)
                .compact();
    }

    public String generateToken (UserDetails userDetails) {
        return generateToken(new HashMap<>(), userDetails);
    }

    public Boolean isTokenValid (String token, UserDetails userDetails) {
        final String _username = getAccessToken(token);
        return (_username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    public Boolean isTokenExpired (String token) {
        return getExpirationDate (token).before(new Date());
    }

    private Date getExpirationDate (String token) {
        return extractClaims(token, Claims::getExpiration);
    }
}
