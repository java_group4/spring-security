package com.springsecurity.constants;

public class Constant {
    public static final Long EXPIRATION = System.currentTimeMillis() + 1000 * 60 * 24;
}
