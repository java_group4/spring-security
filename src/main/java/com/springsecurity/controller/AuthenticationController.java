package com.springsecurity.controller;

import com.springsecurity.model.dao.AuthenticationResponse;
import com.springsecurity.model.dto.AuthenticationRequest;
import com.springsecurity.services.student.StudentService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/v1/auth")
@AllArgsConstructor
public class AuthenticationController {
    private final StudentService _service;
    @Operation(operationId = "login", summary = "userLogin", description = "userLogin")
    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticationResponse> authenticate (@Valid @RequestBody AuthenticationRequest body) {
        return ResponseEntity.ok(_service.authenticate(body));
    }
}
