package com.springsecurity.model.roles;

public enum Roles {
    STUDENT,
    TEACHER
}
